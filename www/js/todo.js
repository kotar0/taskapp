app.controller('MainController', ['$scope', function ($scope) {
	$scope.title = 'TaskApp';
	$scope.tasks = [
		{
			"body": "タスクを追加してみよう",
			"done": false,
			
		}
		
    ];
	$scope.addNew = function () {
		var date = new Date();
		$scope.tasks.push({
			"body": $scope.newTaskBody,
			"done": false,
			"date": date
		});
		$scope.newTaskBody = '';
	}
}]);

//comment